%
% FIELD II pt target sims for testing frequency domain beamformers
%
% written on 07/31/2022
%
% by Marko Jakovljevic
%

field_init(-1)


%%
%%%%%%%%%%%%%%%%%%%%%
% GENERATE THE DATA %
%%%%%%%%%%%%%%%%%%%%%

% set parameters
fs = 100e6;
c = 1540;
set_field('c',c);
set_field('fs',fs);

% define chirp pulse
f0 = 4e6;
lambda = c/f0;
fbwd = 0.65;
BW = fbwd * f0; % bandwidth of the pulse [Hz]
% f_init = f0 - BW/2;
f_init = f0;
% TBP = 50; % time-BW product to set pulse compression
% t_end = TBP/(BW/2);
t_end = 20e-6;
Kr = BW/2/t_end;

t = 0:1/fs:t_end;
chirp_pulse = sin(2*pi*(f_init*t+Kr*t.^2));
% clear t

f_max = f0 + Kr .* t(end);  % Maximum frequency of chirp signal
fc = (f_init + f_max)/2;  % center frequency (effective) 

f2 = figure;
plot(t*1e6,chirp_pulse,'b','LineWidth',1.5);
axis tight; xlim([0 t_end]*1e6)

xlabel ('Time (us)','FontSize', 16)
ylabel ('Voltage (V)','FontSize', 16)
title('Chirp Transmit Pulse','FontSize', 16)

s1 = gca;
set(s1, 'FontSize', 16)
set(s1, 'LineWidth', 1.5)

set(s1, 'XColor', [0.25 0.25 0.25])
set(s1, 'YColor', [0.25 0.25 0.25])
set(s1, 'ZColor', [0.25 0.25 0.25])

kerf = 0.035e-3;
width = c/f0/2 - kerf; % lambda/2 pitch
element_height = 6/1000;
N_elements = 128;
n_sub_x = 1;
n_sub_y = 5;

ap_size = N_elements*(kerf+width);
pitch = kerf + width;

% Generate transmit aperture
transmit_aperture = xdc_linear_array (N_elements, width, element_height, kerf, n_sub_x, n_sub_y,[0 0 100]);
receive_aperture = xdc_linear_array (N_elements, width, element_height, kerf, n_sub_x, n_sub_y,[0 0 100]);

% Tx and Rx element positions
tx_apt_data = xdc_get(transmit_aperture,'rect');
txAptPos = tx_apt_data(24:26,1:(n_sub_x*n_sub_y):end);
txAptPos = txAptPos';

rx_apt_data = xdc_get(receive_aperture,'rect');
rxAptPos = rx_apt_data(24:26,1:(n_sub_x*n_sub_y):end);
rxAptPos = rxAptPos';

% define impulse response
impulse_response = makeImpulseResponse(fbwd,fc,fs);
xdc_impulse (transmit_aperture, impulse_response); % define impusle response of the emit aperture
xdc_impulse (receive_aperture, impulse_response); % define impusle response of the receive aperture

excitation = chirp_pulse;
xdc_excitation(transmit_aperture, excitation);

% time_shift
tshift = (size(conv(impulse_response,conv(impulse_response, ...
       excitation)),2)/2)/fs;

%  create point scatterers
z_points = [10 20 30 30 40]*1e-3;
x_points = [0 0 5 0 -5]*1e-3;
y_points = zeros(size(z_points));

points = [x_points' y_points' z_points'];
amplitudes = ones(size(points,1),1);

% getting single-channel data
[scat, start_times]= calc_scat_all(transmit_aperture, receive_aperture, points, amplitudes, 1);

% full_sch_data has the shape: axial x (Rx_chan x Tx_chan)
full_sch_data = reshape(scat,[size(scat,1), N_elements,N_elements]);

% extract monostatic subset
sch_data_mono = zeros(size(full_sch_data,1),N_elements);
for ii=1:N_elements
    
    sch_data_mono(:,ii) = full_sch_data(:,ii,ii);
    
end

time = [0:size(full_sch_data,1)-1]*1/fs + start_times - tshift; % time vector
