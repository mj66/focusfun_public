function [i_data, q_data] = iq_demod(rf_data, f_demod, f_s)
%
% Demodulates RF signal and returns I and Q components
%
% INUPTS:
%           - rf_data: input rf signal as T x Y (time x chan_no)
%           - f_demod: demodulation frequency (Hz)
%           - f_s: sampling frequency (Hz)
%
% last updated on 12/04/2014
%
% by Marko Jakovljevic

time = [0:1:size(rf_data,1)-1]*1/f_s;

i_data_term = 2*rf_data .* repmat(cos(2*pi*f_demod*time'),[1,size(rf_data,2)]);
q_data_term = 2*rf_data .* repmat(-sin(2*pi*f_demod*time'),[1,size(rf_data,2)]);

% IIR filter design
n_taps = 10;
f_cutoff = f_demod / (f_s/2); % cuto-off at RF freq
[b,a] = butter(n_taps,f_cutoff);

i_data = filtfilt(b,a,i_data_term);
q_data = filtfilt(b,a,q_data_term);

return
