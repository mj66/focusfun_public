function defoc_data = defocus_data(t,foc_data,foci_rx,elempos,varargin)

% DEFOCUS_DATA - De-focuses delayed RF data from focused transmit
%
% INPUTS:
% t                  - T x 1 time vector for samples of the output (defocused) data
% foc_data           - M x N matrix containing delayed RF data to be defocused
% foci_rx            - M x 3 matrix with position of Rx focal points of interest [m]
% elempos            - N x 3 matrix with element positions [m]
% tx_center          - 1 x 3 vector with the position of the center of the Tx aperture
%                    - (Tx center); [0,0,0] by default  
% speed_of_sound     - speed of sounds [m/s]; default 1540 m/s
%
% OUTPUT:
% defoc_data - T x N vector with defocused (RF) data points
%
% written by Marko Jakovljevic
% last updated on 10/30/2023 
%

if nargin == 4
    tx_center = [0 0 0];
    speed_of_sound = 1540;
elseif nargin == 5
    tx_center = varargin{1};
    speed_of_sound = 1540;
elseif nargin == 6
    tx_center = varargin{1};
    speed_of_sound = varargin{2};
else
    error('Improper argument list');
end

tx_center = [tx_center(:)]';

% time from the Rx focus to array elements
rx_times = calc_times(foci_rx,elempos,0,speed_of_sound);

% time from the array elements to Rx focus
tx_distance = foci_rx-repmat(tx_center,size(foci_rx,1),1);
tx_times = repmat(sqrt(sum(tx_distance.^2,2))/speed_of_sound,1,size(elempos,1));

foc_times = rx_times + tx_times;

defoc_data = zeros(size(foc_times));
for ii=1:size(foc_times,2)
    defoc_data(:,ii) = interp1(foc_times(:,ii),foc_data(:,ii),t,'pchip',0);
end

% x_vec = 1:size(signal,2);
% x_mtx = repmat(x_vec,size(foc_times,1),1);
% if length(x_vec) ~=1
%     foc_data = interp2(x_vec,t,signal,x_mtx,foc_times,'cubic',0);
% else
%     foc_data = interp1(t,signal,foc_times,'spline',0);
% end

return