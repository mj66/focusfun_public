%
% FIELD II pt target sims for testing frequency domain beamformers
%
% written on 07/31/2022
%
% by Marko Jakovljevic
%

field_init(-1)


%%
%%%%%%%%%%%%%%%%%%%%%
% GENERATE THE DATA %
%%%%%%%%%%%%%%%%%%%%%

% set parameters
f0 = 5e6;
fs = 20e6;
c = 1540;
lambda = c/f0;
kerf = 0.035e-3;
width = c/f0/2 - kerf; % lambda/2 pitch
element_height = 6/1000;
N_elements = 128;
n_sub_x = 1;
n_sub_y = 5;
fbwd = 1;
set_field('c',c);
set_field('fs',fs);

ap_size = N_elements*(kerf+width);
pitch = kerf + width;

% Generate transmit aperture
transmit_aperture = xdc_linear_array (N_elements, width, element_height, kerf, n_sub_x, n_sub_y,[0 0 100]);
receive_aperture = xdc_linear_array (N_elements, width, element_height, kerf, n_sub_x, n_sub_y,[0 0 100]);

% Tx and Rx element positions
tx_apt_data = xdc_get(transmit_aperture,'rect');
txAptPos = tx_apt_data(24:26,1:(n_sub_x*n_sub_y):end);
txAptPos = txAptPos';

rx_apt_data = xdc_get(receive_aperture,'rect');
rxAptPos = rx_apt_data(24:26,1:(n_sub_x*n_sub_y):end);
rxAptPos = rxAptPos';

% define impulse response
impulse_response = makeImpulseResponse(fbwd,f0,fs);
xdc_impulse (transmit_aperture, impulse_response); % define impusle response of the emit aperture
xdc_impulse (receive_aperture, impulse_response); % define impusle response of the receive aperture

excitation = sin(2*pi*f0*(0:1/fs:2/f0));
xdc_excitation(transmit_aperture, excitation);

% time_shift
tshift = (size(conv(impulse_response,conv(impulse_response, ...
       excitation)),2)/2)/fs;

%  create point scatterers
z_points = [10 20 30 30 40]*1e-3;
x_points = [0 0 5 0 -5]*1e-3;
y_points = zeros(size(z_points));

points = [x_points' y_points' z_points'];
amplitudes = ones(size(points,1),1);

% getting single-channel data
[scat, start_times]= calc_scat_all(transmit_aperture, receive_aperture, points, amplitudes, 1);

% full_sch_data has the shape: axial x (Rx_chan x Tx_chan)
full_sch_data = reshape(scat,[size(scat,1), N_elements,N_elements]);

% extract monostatic subset
sch_data_mono = zeros(size(full_sch_data,1),N_elements);
for ii=1:N_elements
    
    sch_data_mono(:,ii) = full_sch_data(:,ii,ii);
    
end

time = [0:size(full_sch_data,1)-1]*1/fs + start_times - tshift; % time vector
