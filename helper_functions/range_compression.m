function compressed_signal = range_compression(data, excitation) 
    % Performs time-domain range compression 
    % Inputs: 
    %   data - array with RF data for which range compression is needed in
    %           the first dimension 
    %   excitation - vector with transmit signal, assuming this is real
    % Outputs
    %   compressed_signal - array with RF data that has been range-compressed
    %
    % written by Louise Zhuang
    % last updated on 02/26/2023
    
    % Keep original size of input array
    size_orig = size(data); 
    
    % If data is a row vector, make it a column vector to keep operating
    % dimensions consistent
    if isrow(data)
        data = data'; 
    end
    
    % Reshape matrix into 2D 
    data = reshape(data, size(data, 1), []); 
    
    % Perform range compression 
    compressed_data = zeros(size(data));  % Keeps compressed signals in 2D array
    for n = 1:size(data, 2)
        compressed_data(:,n) = conv(data(:,n), fliplr(excitation), 'same'); 
    end
    
    % Reshape array to its original size
    compressed_signal = reshape(compressed_data, size_orig); 
end
