function foc_data = focus_data_pw(t,signal,foci_rx,elempos,alphas,varargin)

% FOCUS_DATA_PW - Focuses the RF data from a plane-wave transmit
%
% The function interpolates the RF signals collected from a plane-wave transmit sequence
% to focus the data at desired locations.
%
% INPUTS:
% t                  - T x 1 time vector for samples of the input signal
% signal             - T x N x M matrix containing input RF data to be interpolated
% elempos            - N x 3 matrix with element positions [m]
% alphas             - M x 1 vector of plane-wave angles w.r.t. x-axes [degrees]
% foci_rx            - P x 3 matrix with position of Rx focal points of interest [m]
%
% tx_center          - 1 x 3 vector with the position of the center of the Tx aperture
%                    - (i.e. origin of the plane-waves); [0,0,0] by default  
% dc_rx                 - time offset [s] for Rx elements; scalar, or N x 1 vector, or P x N matrix
% dc_pw                 - time offset [s] for plane waves; scalar, or M x 1 vector, or P x M matrix
% speed_of_sound     - speed of sounds [m/s]; default 1540 m/s
%
% OUTPUT:
% foc_data - P x N x M vector with interpolated (RF) data points
%
% NOTE: On Verasonics scanner time starts from the edge of the array
% so dc_pw = abs(sind(alphas)*(elempos(end,1)-tx_center(1)))/speed_of_sound
%
%
% written by Marko Jakovljevic
% last updated on 05/26/2022 
%

if nargin == 5
    tx_center = [0 0 0];
    dc_rx = 0;
    dc_pw = 0;
    speed_of_sound = 1540;
elseif nargin == 6
    tx_center = varargin{1};
    dc_rx = 0;
    dc_pw = 0;
    speed_of_sound = 1540;
elseif nargin == 7
    tx_center = varargin{1};
    dc_rx = varargin{2};
    dc_pw = 0;
    speed_of_sound = 1540;
elseif nargin == 8
    tx_center = varargin{1};
    dc_rx = varargin{2};
    dc_pw = varargin{3};
    speed_of_sound = 1540;
elseif nargin == 9
    tx_center = varargin{1};
    dc_rx = varargin{2};
    dc_pw = varargin{3};
    speed_of_sound = varargin{4};
else
    error('Improper argument list');
end

tx_center = [tx_center(:)]';

% check for the number of non-singelton dims (i.e. if it's a vector)
% do not change if scalar or matrix
if sum(size(dc_pw)~=1) == 1
    dc_pw = dc_pw(:);
    dc_pw = repmat(dc_pw', [size(foci_rx,1), 1]);
end

% time from the Rx focus to array elements
rx_times = calc_times(foci_rx,elempos,dc_rx,speed_of_sound);

% time for plane-wave from Tx center to Rx focus
cos_alphas = repmat([cosd(alphas(:))].',size(foci_rx,1),1);
sin_alphas = repmat([sind(alphas(:))].',size(foci_rx,1),1);

tx_distance = cos_alphas .* repmat(foci_rx(:,3)- tx_center(3),1,length(alphas)) + ...
sin_alphas .* repmat(foci_rx(:,1)- tx_center(1),1,length(alphas)); % x and z dims

tx_times = tx_distance/speed_of_sound + dc_pw;

% rx_times is the faster changing dimension
rx_times_tmp = repmat(rx_times,1,size(tx_times,2));
% tx_times is the slower changing dimension
tx_times_tmp = repmat(tx_times,size(rx_times,2),1);
tx_times_tmp = reshape(tx_times_tmp,size(tx_times,1),[]);

foc_times = rx_times_tmp + tx_times_tmp;

signal = reshape(signal,size(signal,1),size(elempos,1)*length(alphas));

x_vec = 1:size(signal,2);
x_mtx = repmat(x_vec,size(foc_times,1),1);

if length(x_vec) ~=1
    foc_data = interp2(x_vec,t,signal,x_mtx,foc_times,'cubic',0);
else
    foc_data = interp1(t,signal,foc_times,'spline',0);
end

foc_data = reshape(foc_data,size(foc_data,1),size(elempos,1),length(alphas));

