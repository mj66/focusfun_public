function foc_data = focus_data(t,signal,foci_rx,elempos,varargin)

% FOCUS_DATA - Focuses the RF data at desired locations
%
% The function interpolates the RF signals received on individual array elements
% to focus the data at desired locations.
%
% INPUTS:
% t                  - T x 1 time vector for samples of the input signal
% signal             - T x N matrix containing input RF data to be interpolated
% foci_rx            - M x 3 matrix with position of Rx focal points of interest [m]
% elempos            - N x 3 matrix with element positions [m]
% tx_center          - 1 x 3 vector with the position of the center of the Tx aperture
%                    - (Tx center); [0,0,0] by default  
% dc                 - time offset [s]; scalar, N x 1 vector, M x N matrix
% speed_of_sound     - speed of sounds [m/s]; default 1540 m/s
%
% OUTPUT:
% foc_data - M x N vector with interpolated (RF) data points
%
% NOTE: Intended to focus the data from diverging (transmit) waves or
% to focus the data along the transmit beam (a single A-line per Tx event)
%
%
% written by Marko Jakovljevic
% last updated on 05/26/2022 
%

if nargin == 4
    tx_center = [0 0 0];
    dc = 0;
    speed_of_sound = 1540;
elseif nargin == 5
    tx_center = varargin{1};
    dc = 0;
    speed_of_sound = 1540;
elseif nargin == 6
    tx_center = varargin{1};
    dc = varargin{2};
    speed_of_sound = 1540;
elseif nargin == 7
    tx_center = varargin{1};
    dc = varargin{2};
    speed_of_sound = varargin{3};
else
    error('Improper argument list');
end

tx_center = [tx_center(:)]';

% time from the Rx focus to array elements
rx_times = calc_times(foci_rx,elempos,dc,speed_of_sound);

% time from the array elements to Rx focus
tx_distance = foci_rx-repmat(tx_center,size(foci_rx,1),1);
tx_times = repmat(sqrt(sum(tx_distance.^2,2))/speed_of_sound,1,size(elempos,1));

foc_times = rx_times + tx_times;

x_vec = 1:size(signal,2);
x_mtx = repmat(x_vec,size(foc_times,1),1);
if length(x_vec) ~=1
    foc_data = interp2(x_vec,t,signal,x_mtx,foc_times,'cubic',0);
else
    foc_data = interp1(t,signal,foc_times,'spline',0);
end
    