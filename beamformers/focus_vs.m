function foc_data = focus_vs(t,signal,foci_rx,rxAptPos,foci_vs,varargin)

% FOCUS_VS - Focuses the RF data from virtual sources
%
% The function interpolates the RF signals received on individual array elements
% to focus the data from a VS (or focused beam) at desired off-axis locations
%
% INPUTS:
% t                  - T x 1 time vector for samples of the input signal
% signal             - T x M x N matrix containing input RF data to be interpolated
% foci_rx            - P x 3 matrix with position of Rx focal points of interest [m]
% rxAptPos           - M x 3 matrix with Rx element positions [m]
% foci_vs            - N x 3 matrix with position of virtual sources [m]
% tx_center          - N x 3 vector with the position of the Tx-aperture center(s)
%                    - (Tx center); [0,0,0] by default  
% dc_rx, dc_tx       - time offsets [s] for VS and Rx; scalars, N (M) x 1 vectors, or P x N (M) matrix
% speed_of_sound     - speed of sounds [m/s]; default 1540 m/s
% hybrid_delay_flag  - flag for a hybrid delay model: 0 or 1; works only for linear scans
% m                  - distance away from the VS to apply plane-wave model
%
% OUTPUT:
% foc_data - P x M x N vector with interpolated (RF) data points
%
%
% written by Marko Jakovljevic
% last updated on 05/26/2022 
%

switch nargin
    case 5
        tx_center = [0 0 0];
        dc_rx = 0; dc_tx = 0;
        speed_of_sound = 1540;
        hybrid_delay_flag = 0;
    case 6
        tx_center = varargin{1};
        dc_rx = 0; dc_tx = 0;
        speed_of_sound = 1540;
        hybrid_delay_flag = 0;
    case 7
        tx_center = varargin{1};
        dc_rx = varargin{2}; dc_tx = 0;
        speed_of_sound = 1540;
        hybrid_delay_flag = 0;
    case 8
        tx_center = varargin{1};
        dc_rx = varargin{2}; dc_tx = varargin{3};
        speed_of_sound = 1540;
        hybrid_delay_flag = 0;
    case 9
        tx_center = varargin{1};
        dc_rx = varargin{2}; dc_tx = varargin{3};
        speed_of_sound = varargin{4};
        hybrid_delay_flag = 0;
    case 10
        tx_center = varargin{1};
        dc_rx = varargin{2}; dc_tx = varargin{3};
        speed_of_sound = varargin{4};
        hybrid_delay_flag = varargin{5};
        m = 1e-3;
    case 11
        tx_center = varargin{1};
        dc_rx = varargin{2}; dc_tx = varargin{3};
        speed_of_sound = varargin{4};
        hybrid_delay_flag = varargin{5};
        m = varargin{6};
    otherwise
        error('Improper argument list');
end

no_rx = size(foci_rx,1);
no_vs = size(foci_vs,1);
no_ele = size(rxAptPos,1);

% check if it's a vector
if size(tx_center,1) == 1
    tx_center = repmat(tx_center, [size(foci_vs,1), 1]);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% time from the Rx focus to array elements %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
rx_times = calc_times(foci_rx,rxAptPos,dc_rx,speed_of_sound);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% time from the virtual sources to Rx focus %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tx_times_pt1 = calc_times(foci_rx,foci_vs,dc_tx,speed_of_sound);

% mask for VS relative to recon-grid point (VS above or below the reconstructed point)
sign_vs = -1*ones(no_rx,no_vs);
[FOCIVS, FOCIRX] = meshgrid(foci_vs(:,3),foci_rx(:,3));
sign_vs(FOCIRX>=FOCIVS) = 1;
tx_times_pt1 = tx_times_pt1 .* sign_vs;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plane-wave model close to VS %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if hybrid_delay_flag == 1
    
    pw_idc = abs(FOCIRX - FOCIVS) <= m;
    tx_times_pt1(pw_idc) = (FOCIRX(pw_idc) - FOCIVS(pw_idc)) / speed_of_sound;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% time from Tx aperture/center to VS %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
r = foci_vs - tx_center;
vs_offset = sqrt(sum(r.^2,2)) / speed_of_sound;

% mask for VS relative to transmit aperture (VS in front or behind the transducer)
sign_tx = -1*ones(no_vs,1);
sign_tx(foci_vs(:,3)>=tx_center(:,3)) = 1;
vs_offset = vs_offset .* sign_tx;
tx_times = tx_times_pt1 + repmat(vs_offset',no_rx,1); % include VS offset in Tx times

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% compute total focus times %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% rx_times is the faster changing dimension
rx_times_tmp = repmat(rx_times,1,size(tx_times,2));
% tx_times is the slower changing dimension
tx_times_tmp = repmat(tx_times,size(rx_times,2),1);
tx_times_tmp = reshape(tx_times_tmp,size(tx_times,1),[]);
foc_times = rx_times_tmp + tx_times_tmp;

%%%%%%%%%%%%%%%%%%%%%%%%
% interpolate the data %
%%%%%%%%%%%%%%%%%%%%%%%%
signal = reshape(signal,size(signal,1),no_ele*no_vs);

x_vec = 1:size(signal,2);
x_mtx = repmat(x_vec,size(foc_times,1),1);
if length(x_vec) ~=1
    foc_data = interp2(x_vec,t,signal,x_mtx,foc_times,'cubic',0);
else
    foc_data = interp1(t,signal,foc_times,'spline',0);
end

foc_data = reshape(foc_data,no_rx,no_ele,no_vs);

return