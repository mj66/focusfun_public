Copyright 2022, Marko Jakovljevic and Louise Zhuang

This source code is licensed under the Apache License, Version 2.0.
The copy of the License can be found at
    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
IN NO EVENT SHALL THE AUTHORS AND/OR STANFORD UNIVERSITY BE LIABLE FOR ANY
SPECIAL, INCIDENTAL, INDIRECT, OR CONSEQUENTIAL DAMAGES OF ANY KIND.
See the License for the specific language governing permissions and
limitations under the License.
