function delta_pw = calc_pw_delays(x,alpha,varargin)

%
% compute plane-wave delays given the element posistion in x-dimension and plane-wave angle
% INPUTS:
% x                 - N x 1 vector of element position in lateral dimension
% alpha             - plane wave angle [degrees]
%
% OUTPUT:
% delta_pw          - delta_pw N x 1 vector of delays w.r.t. to tx_center [s]
%
%
% written by Marko Jakovljevic
% last updated on 05/26/2022 
%

% check for inputs
if nargin == 2
    tx_center = 0;
    speed_of_sound = 1540;
elseif nargin == 3
    tx_center = varargin{1};
    speed_of_sound = 1540;
elseif nargin == 4
    tx_center = varargin{1};
    speed_of_sound = varargin{2};
else
    error('Improper argument list');
end

delta_pw = (x-tx_center) * sind(alpha)/speed_of_sound;

return