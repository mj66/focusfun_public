function apod_matrix = apod_data(elempos,foci_rx,fNum,varargin)

% APOD_DATA - Apodizes focused RF channel data
%
% INPUTS:
% elempos            - N x 3 matrix with element positions [m]
% foci_rx            - P x 3 matrix with position of Rx focal points of interest [m]
% fNum               - scalar, F number to determine the acceptance angle
%
% off_val            - value for elements to be turned off
%
% NOTE: LIMITS ACCEPTANCE ANGLE TO ELIMINATE NEAR-FIELD NOISE; i.e. growing aperture
%
% OUTPUT:
% apod_matrix - P x N matrix with 0's and 1's
%
%
% written by Marko Jakovljevic
% last updated on 05/26/2022 
%

if nargin == 3
    off_val = 1e-4;
elseif nargin == 4
    off_val = varargin{1};
else
    error('Improper argument list');
end

apod_matrix = zeros(size(foci_rx,1),size(elempos,1));

[foci_Z,elempos_Z] = meshgrid(foci_rx(:,3),elempos(:,3));
z_pos = abs(foci_Z - elempos_Z);

[foci_X,elempos_X]  = meshgrid(foci_rx(:,1),elempos(:,1));
x_pos = abs(foci_X - elempos_X);

on_idc = (x_pos == 0) | (z_pos./(2*x_pos) >= fNum);

apod_matrix(on_idc') = 1;
apod_matrix(~on_idc') = off_val; % adopted from DW to prevent NaNs in SLSC images

return