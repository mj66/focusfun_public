function foci = make_beam(range_start,range_end,beam_direction,beam_origin,foc_spacing)

% make_beam - outputs focal positions along the desired beam direction 
%
% Initially intended for creating the focal points along the Rx beam
%
% INPUTS:
% range_start       - scalar for range start of the beam [m]
% range_end         - scalar for range end of the beam [m]
% beam_direction    - 1 x 3 vector determining the beam direction;
%                     x, y, z components do not have to be normalized
% beam_origin       - 1 x 3 vector for beam origin
% foc_spacing       - spacing of points along the beam [m]; affects nuumber of focal points M
%
% OUTPUT:
% foci              - M x 3 matrix with position of beam focal points [m]
%
%
% written by Marko Jakovljevic
% last updated on 05/26/2022 
%

% ensure that dimensions are 1 x 3
beam_origin = [beam_origin(:)]'; 
beam_direction = [beam_direction(:)]';

range = range_start:foc_spacing:range_end;

foci = range' * beam_direction/norm(beam_direction) + repmat(beam_origin,length(range),1);

return;
