%
% Creating B-mode images from linear scan w/ focused Tx
% to test conventional DAS and DAS for virtual sources (VS)
%
% written by Marko Jakovljevic
% on 10/30/2023
%

% set paths
clear all
addpath(genpath('../'))


%%
%%%%%%%%%%%%%%%%%%%%%
% GENERATE THE DATA %
%%%%%%%%%%%%%%%%%%%%%
field_init (-1)
VISUAL = 0;

% set parameters
f0 = 5e6;
fs = 20e6;
c = 1540;
lambda = c/f0;
kerf = 0.035e-3;
width = c/f0/2 - kerf; % lambda pitch
element_height = 6/1000;
N_elements = 64;
n_sub_x = 5;
n_sub_y = 5;
fbwd = 0.6;
set_field('c',c);
set_field('fs',fs);

ap_size = N_elements*(kerf+width);
pitch = kerf + width;

% Tx and Rx apertures of the same size
transmit_aperture = xdc_linear_array (N_elements, width, element_height, kerf, n_sub_x, n_sub_y, [0 0 1000]);
receive_aperture = xdc_linear_array (N_elements, width, element_height, kerf, n_sub_x, n_sub_y,[0 0 1000]);

% Tx and Rx element positions
tx_apt_data = xdc_get(transmit_aperture,'rect');
txAptPos = tx_apt_data(24:26,1:(n_sub_x*n_sub_y):end);
txAptPos = txAptPos';

rx_apt_data = xdc_get(receive_aperture,'rect');
rxAptPos = rx_apt_data(24:26,1:(n_sub_x*n_sub_y):end);
rxAptPos = rxAptPos';

% define impulse response
impulse_response = makeImpulseResponse(fbwd,f0,fs);
xdc_impulse (transmit_aperture, impulse_response); % define impusle response of the emit aperture
xdc_impulse (receive_aperture, impulse_response); % define impusle response of the receive aperture

excitation = sin(2*pi*f0*(0:1/fs:2/f0));
xdc_excitation(transmit_aperture, excitation);

% time_shift
tshift = (size(conv(impulse_response,conv(impulse_response, ...
       excitation)),2)/2)/fs;

% create point scatterers
z_points = [10 20 30 30 40 50]*1e-3;
x_points = [0 0 5 0 -5 5]*1e-3;
y_points = zeros(size(z_points));

points = [x_points' y_points' z_points'];
amplitudes = ones(size(points,1),1);

% FOV
% shift the beam/aperture by one element at time
nlines_x = 128;
rx_x = (0:nlines_x-1) * diff(rxAptPos(1:2,1));
rx_x = rx_x - mean(rx_x);

% focus Tx beam
Tx_focus = [0 0 2e-2];
tx_times = calc_times(Tx_focus,txAptPos,0,c);
if Tx_focus(3) < mean(txAptPos(:,3)) % Tx focus above the array
    delays = tx_times - max(tx_times);
elseif Tx_focus(3) > mean(txAptPos(:,3)) % Tx focus below the array
    delays = -(tx_times - min(tx_times));
end
xdc_times_focus(transmit_aperture, 0, delays);

delays = xdc_get(transmit_aperture, 'focus');
figure; plot(txAptPos(:,1)*1e3,delays(2:end)*1e9); axis tight; grid on
xlabel('Tx ele position (mm)'); ylabel('Tx delays (ns)'); title('Tx focus delay profile')

% initialize variables
max_depth = z_points(end);
max_time = 2.5*max_depth/c;
time_q = [0:round(max_time*fs)-1]*1/fs; %  + start_times - tshift; % time vector for the edge position

N_elements_large = N_elements+(nlines_x-1);

sch_data = zeros(length(time_q),N_elements,nlines_x); % include only active aperture

for ii=1:nlines_x

    disp(['Simulated line #' num2str(ii)])
    
    % shift the targets
    offset = [-rx_x(ii) 0 0];
    points_tmp = points + repmat(offset,size(points,1),1);
    [scat, start_time_tmp] = calc_scat_multi(transmit_aperture, receive_aperture, points_tmp, amplitudes);    
    
    time = [0:size(scat,1)-1]*1/fs + start_time_tmp - tshift; % time vector
    
    scat_q = interp1(time,scat,time_q,'pchip',0);
    
    sch_data(:,:,ii) = scat_q;
    
end

%%
%%%%%%%%%%%%%%%%%%%%%%%
% Conventional DAS BF %
%%%%%%%%%%%%%%%%%%%%%%%

% DAS for Rx beams
ax_span = time_q*c/2;
foc_spacing = ax_span(2) - ax_span(1);
range_start = ax_span(1);
range_end = ax_span(end);
range_axes = range_start:foc_spacing:range_end;
num_foci = length(range_axes);

% beamform using active aperture only
beams = zeros(num_foci,nlines_x);
for ii=1:nlines_x

    beam_origin = [mean(rxAptPos(:,1)) 0 0];
    beam_direction = [0 0 1];
    foci_rx = make_beam(range_start,range_end,beam_direction,beam_origin,foc_spacing);

    dc = 0;
    tx_center = beam_origin;
    foc_data = focus_data(time_q, sch_data(:,:,ii), foci_rx, rxAptPos, tx_center, dc, c);
    
    defoc_data = defocus_data(time_q,foc_data,foci_rx,rxAptPos,tx_center,c);
    
    if VISUAL & ii==round(nlines_x/2)
        figure;
        subplot(1,3,1);
        imagesc(rxAptPos(:,1)*1e3,range_axes*1e3,foc_data); axis image; colormap(gray)
        xlabel('Rx element position (mm)'); ylabel('range (mm)'); title('Focused RF data')
        subplot(1,3,2);
        imagesc(rxAptPos(:,1)*1e3,time_q*c/2*1e3,sch_data(:,:,ii)); axis image; colormap(gray)
        xlabel('Rx element position (mm)'); title('Unfocused RF data')
        subplot(1,3,3);
        imagesc(rxAptPos(:,1)*1e3,time_q*c/2*1e3,defoc_data); axis image; colormap(gray)
        xlabel('Rx element position (mm)'); title('Defocused RF data')
    end
    
    beams(:,ii) = sum(foc_data,2);

end

env = abs(hilbert(beams));
env = env/max(env(:));

%%
%%%%%%%%%%%%%
% VS DAS BF %
%%%%%%%%%%%%%
foci_vs = [rx_x', zeros(length(rx_x),1), ones(length(rx_x),1)*Tx_focus(3)];

beams_vs = zeros(num_foci,nlines_x);
dc = 0;
if VISUAL; figure; end %#ok<*UNRCH>
tic
tmp = zeros(length(range_axes),size(rxAptPos,1),length(rx_x));
for jj=1:length(rx_x) % go through data from each firing
    
    disp(['Beamformed VS #' num2str(jj)])
    
    offset = rx_x(jj);
    rxAptPos_tmp = rxAptPos+[offset 0 0];
    tx_center = mean(rxAptPos_tmp,1);

    beams_tmp = zeros(size(beams_vs));
    for ii=1:nlines_x % go through image lines to be reconstructed
        beam_origin = [rx_x(ii) 0 0];
        beam_direction = [0 0 1];
        foci_rx = make_beam(range_start,range_end,beam_direction,beam_origin,foc_spacing);
        
        m = 0.25e-3;
        foc_data_tmp = focus_vs(time_q,sch_data(:,:,jj),foci_rx,rxAptPos_tmp,foci_vs(jj,:),tx_center,dc,dc,c,1,m);
        
        beams_tmp(:,ii) = sum(foc_data_tmp,2);    
    end
    
    beams_vs = beams_vs + beams_tmp;
    
    if VISUAL
        subplot(1,2,1); imagesc(rx_x, range_axes, beams_tmp)
        axis image
        title(['Frame' num2str(jj)])
        subplot(1,2,2); imagesc(rx_x, range_axes, beams_vs)
        axis image
        title('Accumulated frame')
        pause()
    end
    
end
toc

%%
%%%%%%%%%%%
% B-modes %
%%%%%%%%%%%
beams = beams/max(beams(:));
env_das = abs(hilbert(beams));
env_das = env_das/max(env_das(:));

beams_vs = beams_vs/max(beams_vs(:));
env_das_vs = abs(hilbert(beams_vs));
env_das_vs = env_das_vs/max(env_das_vs(:));

clim = [-50 0];

figure;
subplot(1,2,1)
imagesc(rx_x,range_axes,db(env_das))
axis image
axis off
ylim([0 60e-3])
xlim([-15 15]*1e-3)
caxis(clim)
title('Focused Tx')

hold on
line([3,8]*1e-3,[64,64]*1e-3, 'LineWidth',1.5,'Color', [0.9 0.9 0.9]) % scalebar

subplot(1,2,2)
imagesc(rx_x,range_axes,db(env_das_vs))
axis image
axis off
ylim([0 60e-3])
xlim([-15 15]*1e-3)
caxis(clim)
colormap(gray)
title('DAS VS')
