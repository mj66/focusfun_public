%
% Creating B-mode images from simulated monostatic and multistatic synthetic aperture data
%
% written on 10/04/2022
%
% by Marko Jakovljevic
%
% updated to simulate chirp trasnmit pulse on 02/26/2023
%

% set paths
clear all
addpath(genpath('../'))

%%
%%%%%%%%%%%%%%%%%%%%%
% GENERATE THE DATA %
%%%%%%%%%%%%%%%%%%%%%
CHIRP_TRANSMIT = 1;
if CHIRP_TRANSMIT
    run genChirpTransmitData
else
    run genFieldIIData %#ok<*UNRCH>
end

%%
% DAS

% pulse compression
if CHIRP_TRANSMIT
    
    % Gaussian window
    L = length(excitation);
    alpha = 1.5; % empirically set the width
    w = gausswin(L,alpha);
    
    hold on
    plot(t*1e6, w' .* excitation,'r')
    legend('Excitation pulse','Matching pulse')
    
    figure;
    subplot(1,2,1);
    imagesc(rxAptPos(:,1)*1e3,time*1e5,sch_data_mono/max(sch_data_mono(:))); colormap(gray); caxis([-1 1])
    ylim([1 2])
    xlabel('lateral (mm)'); ylabel('time(us)')
    title('Before matched filtering');
    
    full_sch_data = range_compression(full_sch_data, w' .* excitation);
    sch_data_mono = range_compression(sch_data_mono, w' .* excitation);

    subplot(1,2,2);
    imagesc(rxAptPos(:,1)*1e3,time*1e5,sch_data_mono/max(sch_data_mono(:))); colormap(gray); caxis([-1 1])
    ylim([1 2])
    xlabel('lateral (mm)'); ylabel('time(us)')
    title('After matched filtering');
end

ax_span = time*c/2;
foc_spacing = ax_span(2) - ax_span(1);
range_start = ax_span(1);
range_end = ax_span(end);
range_axes = range_start:foc_spacing:range_end;
num_foci = length(range_axes);

x_size = 20e-3;
nlines = 256;
rx_x = (linspace(0,1,nlines)-0.5)*x_size;
dc_rx = 0;
dc_tx = 0;

beams_mono = zeros(num_foci,nlines);
beams_multi = zeros(num_foci,nlines);

% tic
poolobj = parpool('local',8);
parfor ii=1:nlines

    beam_origin = [rx_x(ii) 0 0];
    beam_direction = [0 0 1];
    foc_pts = make_beam(range_start,range_end,beam_direction,beam_origin,foc_spacing);

    % multistatic
    foc_data = focus_fs(time', full_sch_data, foc_pts, rxAptPos, txAptPos, dc_rx, dc_tx, c);
    beams_multi(:,ii) = double(squeeze(sum(sum(foc_data,3),2)));

    % monostatic
    foc_tmp = zeros(num_foci,N_elements);
    for idnum = 1:N_elements        
        foc_tmp(:,idnum) = focus_data(time',sch_data_mono(:,idnum),foc_pts,rxAptPos(idnum,:),txAptPos(idnum,:),dc_rx, c); %#ok<*PFBNS>
    end
    beams_mono(:,ii) = squeeze(sum(foc_tmp,2));
    
    % clear foc_data
    display(['Focused line # ' num2str(ii)])

end
delete(poolobj)
% toc


%%
%%%%%%%%%%%%%%%%%%
% B-mode images %%
%%%%%%%%%%%%%%%%%%

env_das_multi = abs(hilbert(beams_multi));
env_das_multi = env_das_multi/max(env_das_multi(:));

env_das_mono = abs(hilbert(beams_mono));
env_das_mono = env_das_mono/max(env_das_mono(:));

% Figure
clim = [-50 0];
figure('PaperPositionMode', 'auto');
s1 = subplot(1,2,1);
imagesc(rx_x, range_axes, db(env_das_mono));
axis image; axis off;
colormap(gray); caxis(clim)
title('DAS mono')
set(gca,'FontSize',10)

hold on
line([3,8]*1e-3,[41,41]*1e-3, 'LineWidth',1.5,'Color', [0.9 0.9 0.9]) % scalebar

s2 = subplot(1,2,2);
imagesc(rx_x, range_axes, db(env_das_multi));
axis image; axis off;
colormap(gray); caxis(clim)
title('DAS multi')
set(gca,'FontSize',10)
