%
% Creating B-mode images from simulated plane wave data using DAS.
%
% written on 10/04/2022
%
% by Marko Jakovljevic

% set paths
clear all
addpath(genpath('../'))

%%
%%%%%%%%%%%%%%%%%%%%%
% GENERATE THE DATA %
%%%%%%%%%%%%%%%%%%%%%

run genFieldIIData

%%
%%%%%%%%%%%%%%%%%%%%%
% GENERATE PW DATA %%
%%%%%%%%%%%%%%%%%%%%%

totalAngle = 18; % angular span [degrees]
na = 7;
alphas = linspace(-totalAngle/2,totalAngle/2,na); % [degrees]
data_pw = [];

tx_center = (alphas<0) * rxAptPos(end,1) + (alphas>0) * rxAptPos(1,1);

for ii=1:na
    
    % create plane-waves from FSA data
    alpha_tmp = alphas(ii);
    start_distance = sind(alpha_tmp)*(txAptPos(:,1) - tx_center(ii));
    delta_pw = abs(start_distance/c);
    
    % time x Rx_chan x Tx_chan
    data_pw_tmp = zeros(size(full_sch_data,1),size(full_sch_data,2),size(full_sch_data,3));
    
    for chanTxId = 1:length(txAptPos)

        time_pw = time - delta_pw(chanTxId);
        data_pw_tmp(:,:,chanTxId) = interp1(time,squeeze(full_sch_data(:,:,chanTxId)),time_pw,'pchip',0);
    end

    data_pw = cat(3, data_pw, squeeze(sum(data_pw_tmp,3)));
    
    display(['Finished simulation for angle ' num2str(alpha_tmp)])

end


%%
%%%%%%%%%%%%%%%%%%
% FOCUS PW DATA %%
%%%%%%%%%%%%%%%%%%

ax_span = time*c/2;
foc_spacing = ax_span(2) - ax_span(1);
range_start = ax_span(1);
range_end = ax_span(end);
range_axes = range_start:foc_spacing:range_end;
num_foci = length(range_axes);

% Rx foci
x_size = 20e-3;
nlines = 256;
rx_x = (linspace(0,1,nlines)-0.5)*x_size;

beams = zeros(num_foci,na,nlines);

% compute start-times
tx_center = [0 0 0];
start_distance = -sind(alphas)*abs(rxAptPos(end,1)-tx_center(1));
dc_pw = abs(start_distance/c);
dc_rx = 0;

for idnum=1:nlines

    % calculate the foci
    beam_origin = [rx_x(idnum) 0 0];
    beam_direction = [0 0 1];
    foci_rx = make_beam(range_start,range_end,beam_direction,beam_origin,foc_spacing);
    
    foc_data_tmp = focus_data_pw(transpose(time),data_pw,foci_rx,rxAptPos,alphas,tx_center,dc_rx,dc_pw,c);

    beams(:,:,idnum) = squeeze(sum(foc_data_tmp,2));
    
    disp(['Focused line #' num2str(idnum)])
end



%%
%%%%%%%%%%%
% B-MODE %%
%%%%%%%%%%%

env = abs(hilbert(squeeze(sum(beams,2))));
env = env/max(env(:));

figure;
imagesc(rx_x,range_axes,db(env),[-50 0]); axis image; axis off
colormap(gray); colorbar
title(['B-mode for ' num2str(na) ' PW angles'])

